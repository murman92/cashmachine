package com.example.gabriellamurman.cashmachine;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;

public class Deposit extends AppCompatActivity {

    Button btn_deposit2;
    EditText txt_money;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_deposit);

        btn_deposit2 = (Button) findViewById(R.id.btn_deposit2);
        txt_money = (EditText) findViewById(R.id.txt_money);

        btn_deposit2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                Intent i = new Intent(getApplicationContext(), MainActivity.class);
                i.putExtra("money", txt_money.getText().toString());
                i.putExtra("type", "deposit");
                startActivity(i);
                finish();

            }
        });
    }
}

