package com.example.gabriellamurman.cashmachine;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.example.gabriellamurman.cashmachine.model.bancomat;

public class MainActivity extends AppCompatActivity {

    Button btn_deposit;
    Button btn_quit;
    Button btn_withdraw;
    TextView lbl_balance;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        //VARIABLES
        String type;
        Integer money;

        //CHECK BUNDLE
        String[] action = getBundle();
        if (action != null){
            type = action[0];
            money = Integer.parseInt(action[1]);

            Log.d("main_activity", "type:" + type);
            Log.d("main_activity", "money:" + money);


            //LOAD MODEL
            bancomat x = new bancomat(0);
            if (type.equals("deposit")){
                x.PutMoney(money);

                Log.d("main_activity", "balance:" + x.GetBalance());
                lbl_balance = (TextView) findViewById(R.id.lbl_balance);
               // lbl_balance.setText(x.GetBalance());
            }
        }

        //layout & FindViewByIds
        layout_setting();

    }

    public String[] getBundle(){
        String[] action = new String[2];
        try{
            Bundle b = getIntent().getExtras();
            action[0] = b.getString("type");
            action[1] = b.getString("money");
            return action;
        }catch (Exception ex){
            return null;
        }
    }

    public void layout_setting() {


        //BTN DEPOSIT
        btn_deposit = (Button) findViewById(R.id.btn_deposit);
        btn_deposit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent i = new Intent(getApplicationContext(), Deposit.class);
                startActivity(i);

            }
        });

        //BTN WITHDRAW
        btn_withdraw = (Button) findViewById(R.id.btn_withdraw);
        btn_withdraw.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

            }
        });


        //BTN QUIT
        btn_quit = (Button) findViewById(R.id.btn_quit);
        btn_quit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Toast.makeText(getApplicationContext(), "Bye Bye", Toast.LENGTH_LONG).show();
                finish();
                moveTaskToBack(true);

            }
        });
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        getMenuInflater().inflate(R.menu.menu, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {

        int id = item.getItemId();

        if (id == R.id.action_credits) {
            Toast.makeText(getApplicationContext(), "Credit button has been clicked", Toast.LENGTH_LONG).show();
        }

        if (id == R.id.test) {
            Toast.makeText(getApplicationContext(), "Credit button has been clicked", Toast.LENGTH_LONG).show();
        }

        return super.onOptionsItemSelected(item);


    }

}





